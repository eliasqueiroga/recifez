"use strict";
/*jslint node: true */
/*global require */

module.exports = function(app, dbAdapter) {
	
	app.get("/search/:addr", function(req, res) {
		var address = req.params.addr;
		dbAdapter.search(address, function(data) {
			console.log(data);
			res.send(data);
		});
	});

	app.get("/search/hash/:tag", function(req, res) {
		var hash = req.params.tag;
		dbAdapter.searchHash(hash, function(data) {
			res.send(data);
		});
	});
	
};
