"use strict";
/*jslint node: true */
/*global require */

module.exports = function(settings, mongoDb) {

	var adapter = settings.adapter.mongo;
	var mongoServer = new mongoDb.Server(adapter.host, adapter.port, adapter.options, { "native_parser" : false});
	var db;
	var db_connector = new mongoDb.Db(adapter.db, mongoServer);
	
	var responseObj = function() {
		this.type; 
		this.objects;
		this.trecho;
		this.hash;
	};

	db_connector.open(function(err, dbObject) {
		if (err) { console.error("MongoDB: " + err); }

		if (!err) {
			db = dbObject;
			console.log("MongoDB Connected...");
		}
	});

	var replaceAll = function(string, token, newtoken) {
		while (string.indexOf(token) != -1) {
			string = string.replace(token, newtoken);
		}
		return string;
	};
	

	exports.get = function() { return db; };

	exports.search = function(street, callback) {
		street = replaceAll(street, "+", " ");

		db_connector.collection("trechoslogradouros", function(err, trechoslogradouros) {
			var query = {
				NLGPAVOFIC : {
					"$regex" : ".*" + street + ".*",
					"$options" : "-i"
				}
			};

			trechoslogradouros.find(query).toArray(function(err, trechoslogradourosResults) {

				var names = [];
				var resultArray = [];
				for (var r in trechoslogradourosResults) {
					if (names.indexOf(trechoslogradourosResults[r]["NLGPAVOFIC"]) < 0) {
						resultArray.push(trechoslogradourosResults[r]);
						names.push(trechoslogradourosResults[r]["NLGPAVOFIC"]);
					}
				}
			
				if (resultArray.length > 1) {
					var logras = new Array();
					
					for (var i = 0; i < resultArray.length; i++) {
						logras.push(resultArray[i]["NLGPAVOFIC"]);
					}

					var response = new responseObj();
					response.type = 0;
					response.objects = logras;
					response.trecho = null;

					callback(response);
				} else if (resultArray.length == 1) {
					var trecho = resultArray[0];
					var logra = trecho.CLOGRACODI;

					db_connector.collection("FaceQuadra", function(err, FaceQuadra) {

						FaceQuadra.find({LOGRA : logra}, {geometry : 0}).toArray(function(err, FaceQuadraResults) {
							if (callback && typeof (callback) === "function") {

								var hashtag = replaceAll(street, " ", "");
								hashtag = replaceAll(hashtag, "á", "a");
								hashtag = replaceAll(hashtag, "é", "e");
								hashtag = replaceAll(hashtag, "í", "i");
								hashtag = replaceAll(hashtag, "ó", "o");
								hashtag = replaceAll(hashtag, "ú", "u");
								hashtag = replaceAll(hashtag, "ã", "a");
								hashtag = replaceAll(hashtag, "õ", "o");
								hashtag = replaceAll(hashtag, "à", "a");
								hashtag = replaceAll(hashtag, "û", "u");
								hashtag = replaceAll(hashtag, "â", "a");
								hashtag = hashtag.toLowerCase();

								var response = new responseObj();
								response.type = 1;
								response.objects = FaceQuadraResults;
								response.trecho = trecho;
								response.hash = hashtag;
								
								console.log(FaceQuadraResults);

								db_connector.collection("queries", function(err, queries) {
									queries.insert({
										hash : hashtag,
										query : response
									}, {
										safe : true
									}, function(err, records) {
										console.log("Query added");
									});
								});

								callback(response);
							}
						});
					});
				} else if (resultArray.length == 0) {
					var response = new responseObj();
					response.type = 2;
					response.objects = new Array();
					response.trecho = null;

					callback(response);
				}
			});
		});
	};

	exports.searchHash = function(hashTag, callback) {
		hashTag = replaceAll(hashTag, "+", " ");
		console.log(hashTag)
		
		db_connector.collection("queries", function(err, queries) {
			queries.find({hash : hashTag}).toArray(function(err, queriesResults) {
				if (queriesResults.length > 0) {
					callback(queriesResults[0]);
				} else {
					var response = new responseObj();
					response.type = 2;
					response.objects = new Array();
					response.trecho = null;
					callback(response);
				}
			});
		});
	}

	return exports;
};