"use strict";
/*jslint node: true */
/*global require */

module.exports = function (fs, csv, underscore, storeAdapter) {
    exports.importData = function (type, file) {
        var recordKeys;

        process.stdout.write("0 records saved.");

        csv()
            .from.path(file, {
                delimiter: ";",
                escape: "\""
            })
            .transform( function (row) {
                row.unshift(row.pop());
                return row;
            })
            .on("record", function (row, index) {
                if (index === 0) {
                    recordKeys = row;
                }

                if (index !== 0) {
                    storeAdapter.save(type, underscore.object(recordKeys, row));
                }

                process.stdout.clearLine();
                process.stdout.write(index.toString() + " records saved.");
                process.stdout.cursorTo(0);
            })
            .on("end", function (count) {
                console.log(count + " records saved.");
                return 0;
            })
            .on("error", function (error) {
                console.error(error.message);
                return -1;
            });
    };

    return exports;
};
