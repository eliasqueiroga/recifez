#!/usr/bin/env node

"use strict";
/*jslint node: true */
/*global require */

var settings = require("./config/settings.json"),
	 mongoDb = require("mongodb"),
     express = require('express'),
         app = express(),
   dbAdapter = require("./lib/mongo")(settings, mongoDb),
    requests = require("./lib/requests")(app, dbAdapter);

app.use(express.static(settings.server.public));

app.listen(settings.server.port);