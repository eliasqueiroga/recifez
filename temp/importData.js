var mongodb = require("mongodb");
     var fs = require("fs");
    var csv = require("csv");
require('./json/json2.js');

var mongoserver = new mongodb.Server(
	"54.235.206.219", 27017,
	{
		safe : true,
		w : "majority",
		journal : true,
		fsync : true,
		auto_reconnect : false,
		poolSize : 10
	}, 
	{ native_parser : false }
);

var db_connector = new mongodb.Db("recifez", mongoserver);
db_connector.open(function(err, db) {});

var replaceAll = function(string, token, newtoken) {
	while (string.indexOf(token) != -1) {
		string = string.replace(token, newtoken);
	}
	return string;
};

var insert = function(collectionName, data) {
	
	console.log(collectionName);
	console.log(data.length);

	db_connector.collection(collectionName, function(err, coll) {
		coll.insert(data, {safe : true}, function(err, records) {
			if (err) console.log(index + ": " + err + " --------- " + data);
		});
	});
};

exports.readFile = function() {

	var collectionName = "FaceQuadra";
	var file = "FaceQuadra.geojson";
	console.log("Importing " + file);

	try {
		var text = fs.readFileSync("C:\\Users\\rrw\\Dropbox\\Apps\\CPHack\\bitbucket\\temp\\" + file);
		var source = JSON.parse(text);
		var obj;
		
		var contentData = [];
		
		for (var index in source.features) {
			obj = source.features[index].properties;
			obj["geometry"] = source.features[index].geometry;
			contentData.push(obj);
		}
		
		insert(collectionName, contentData);
		
	} catch(ex) {
		console.log(ex);
	}
	
	console.log("processo concluido");
};