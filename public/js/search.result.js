// Event handler for search close button icon click.
$("#closeSearchArea").bind("click", function() {
	resetView();
	$("#searchContent").val("");
});

var checkResultData = function(data) {
	switch(data.type) {
		case 0 : // multiple results
			for (var entry in data.objects) {
				multipleResults.insertEntry(data.objects[entry]);
			}
			$("#hasMultipleResult").toggleClass("hasMultipleResultAdj");
			$("#textTitleResult").html($("#searchContent").val());
			break;
		case 1 : // result found
			$("#hasResult").toggleClass("hasResultAdj");
			$("#textTitleResult").html(data.trecho.NLGPAVOFIC);
			
			var resultHome = document.getElementsByName("resultHome")[0];
			resultHome.innerHTML = data.objects.length;
			
			var resultEnergy = document.getElementsByName("resultEnergy")[0];
			var resultEnergyCount = data.objects.filter(function(index) { return index.REDELETRIC == "COM REDE ELETRICA"; }).length;
			resultEnergy.innerHTML = ((resultEnergyCount / data.objects.length) * 100).toFixed(1) + "%";
			
			var resultWater = document.getElementsByName("resultWater")[0];
			var resultWaterCount = data.objects.filter(function(index) { return index.REDEAGUA == "COM REDE DE AGUA"; }).length;
			resultWater.innerHTML = ((resultWaterCount / data.objects.length) * 100).toFixed(1) + "%";
			
			var resultWaste = document.getElementsByName("resultWaste")[0];
			var resultWasteCount = data.objects.filter(function(index) { return index.REDESGOTO == "COM REDE DE ESGOTO"; }).length;
			resultWaste.innerHTML = ((resultWasteCount / data.objects.length) * 100).toFixed(1) + "%";
			
			var resultPhone = document.getElementsByName("resultPhone")[0];
			var resultPhoneCount = data.objects.filter(function(index) { return index.REDETELEFO == "COM REDE TELEFONICA"; }).length;
			resultPhone.innerHTML = ((resultPhoneCount / data.objects.length) * 100).toFixed(1) + "%";
			
			// Example in how to insert the data on the map.
			insertMap(data.trecho.geometry.coordinates[0][0], 13, data.trecho.geometry.coordinates, 18);
			
			// Changing the OG content.
			var metaContents = document.getElementsByTagName("meta");
			for (var m in metaContents) {
				if (metaContents[m].hasAttribute("property") && metaContents[m].getAttribute("property") == "og:title") {
					metaContents[m].setAttribute("content", "Recifez: Busca por " + data.trecho.NLGPAVOFIC);
					break;
				}
			}
			
			break;
		case 2 : // no result found
			$("#noResult").toggleClass("noResultAdj");
			$("#textTitleResult").html($("#searchContent").val());
			break;
	};
};

// Loading the map content on specific div
var insertMap = function(initialCoord, zoomInitial, polygonCoord, zoomMax) {
	// Defines the map to be displayed.
	var map = L.map("streetMap").setView(initialCoord, zoomInitial);
	
	L.marker(initialCoord).addTo(map);

	// Inserting the tile as a new layer on the map.
	L.tileLayer("http://otile{s}.mqcdn.com/tiles/1.0.0/osm/{z}/{x}/{y}.png", 
		{ maxZoom: zoomMax, subdomains: "1234" }
	).addTo(map);

	// Defines the polygon.
	L.polygon(polygonCoord).addTo(map);
};

// Multiple results handler.
var multipleResults = {

	// Inserting an entry on the multiple content area.
	insertEntry : function(value) {
		$("#hasMultipleResult").append(this.getEntryElement(value));
	},
	
	// Getting the HTML Element related to the entry being inserted.
	getEntryElement : function(value) {
		var entryResultElement = document.createElement("div");
		entryResultElement.setAttribute("id", "entryResult");
		entryResultElement.setAttribute("class", "entryResult");
		entryResultElement.setAttribute("name", value);
		
		var entryResultIconElement = document.createElement("div");
		entryResultIconElement.setAttribute("id", "entryResultIcon");
		entryResultIconElement.setAttribute("class", "entryResultIcon");
		
		var imgMapElement = document.createElement("image");
		imgMapElement.setAttribute("src", "images/map.png");
		imgMapElement.setAttribute("class", "imgMap");
		
		entryResultIconElement.appendChild(imgMapElement);
		
		var entryResultContentElement = document.createElement("div");
		entryResultContentElement.setAttribute("id", "entryResultContent");
		entryResultContentElement.setAttribute("class", "entryResultContent");
		entryResultContentElement.innerHTML = value;
		entryResultContentElement.addEventListener("click", function(evt) {
			var searchEntry = evt.currentTarget.parentElement.getAttribute("name");
			if (searchEntry) { multipleResults.clickEntry(evt.currentTarget.parentElement, searchEntry); }
		});
		
		entryResultElement.appendChild(entryResultIconElement);
		entryResultElement.appendChild(entryResultContentElement);
		
		return entryResultElement;
	},
	
	// Handling the click event on the entry.
	clickEntry : function(value) {
		$("#hasMultipleResult").toggleClass("hasMultipleResultAdj");
		$("#hasMultipleResult").html("");
		
		searchContent(value.textContent);
	}
};