// Browser window loading procedure handler.
window.onload = function () {

	// Event handler for key press when the user is making a search.
	$("#searchContent").bind("keyup", function(evt) {
		// Clear the timeout before starting next.
		clearTimeout(searchPendingProcedure);
		
		if (event.which == 13 ) { 
			adjSearchArea(true);
		} else {
			adjSearchArea();
		}
	});
	
	// Event handler for mouse click when the user is making a search.
	$("#searchContent").bind("click", function(evt) {
		adjSearchArea();
	});
	
	// Event handler for clicking on the logo.
	$("#logo").click(function() {
		if (isSearchPositionAdjusted) { 
			resetView(); 
			$("#searchContent").val("");
		}
	});

	// Getting randomly the background content.
	getRandomBg();
};

// Minimum amount of key pressing procedures.
var KEY_MIN = 3;

// Searching position adjustment.
var isSearchPositionAdjusted = false;

// Search result opened.
var isSearchResultOpened = false;

// Variable to store the time out procedure.
var searchPendingProcedure;
	
// Enconding/Decoding contents in UTF8 format.
var UTF8 = {
	encode: function(s){
		for(var c, i = -1, l = (s = s.split("")).length, o = String.fromCharCode; ++i < l;
			s[i] = (c = s[i].charCodeAt(0)) >= 127 ? o(0xc0 | (c >>> 6)) + o(0x80 | (c & 0x3f)) : s[i]
		);
		return s.join("");
	},
	decode: function(s){
		for(var a, b, i = -1, l = (s = s.split("")).length, o = String.fromCharCode, c = "charCodeAt"; ++i < l;
			((a = s[i][c](0)) & 0x80) &&
			(s[i] = (a & 0xfc) == 0xc0 && ((b = s[i + 1][c](0)) & 0xc0) == 0x80 ?
			o(((a & 0x03) << 6) + (b & 0x3f)) : o(128), s[++i] = "")
		);
		return s.join("");
	}
};	

// Adjusting the searching area.
var adjSearchArea = function(enterKey) {
	if (isSearchPositionAdjusted && !isSearchResultOpened) {
		resetView();
		return;
	}
	
	if ($("#searchContent").val().length >= KEY_MIN) {
		var timeoutMs = 2500;
		if (enterKey) timeoutMs = 0;
	
		// Wait for three seconds before execute the searching.
		searchPendingProcedure = setTimeout(function() {
			if (!isSearchPositionAdjusted) {
				isSearchPositionAdjusted = true;
				toggleSearchAreas();
				$("#resultArea").addClass("resultAreaAdj");
				isSearchResultOpened = true;

				searchContent($("#searchContent").val());
			}

			// Updating the result content title.
		}, timeoutMs);

	} else {
		if (isSearchPositionAdjusted && isSearchResultOpened) {
			resetView();
		}
	}
};

// Reseting the viewer to the initial content.
var resetView = function() {
	isSearchPositionAdjusted = false;
	isSearchResultOpened = false;
	toggleSearchAreas();
	closeSearchResult();
};

// Adjusting all necessary elements to the right position
// on search procedure.
var toggleSearchAreas = function() {
	$("#logoArea").toggleClass("logoAreaPositionAdj");
	$("#searchArea").toggleClass("searchAreaPositionAdj");
	$("#searchFilterArea").toggleClass("searchFilterAreaPositionAdj");
	$("#searchContentArea").toggleClass("searchContentAreaPositionAdj");
	
	if (isSearchPositionAdjusted) {
		$("#searchFilter").prop("disabled", true);
		$("#searchFilter").css("opacity", 0);
		
	} else {
		$("#searchFilter").removeAttr("disabled");
		$("#searchFilter").css("opacity", 1);
	}
};

// Closing the Search Result area.
var closeSearchResult = function() {
	$("#resultArea").removeClass("resultAreaAdj");

	if ($("#hasResult").hasClass("hasResultAdj"))
		$("#hasResult").toggleClass("hasResultAdj");
		
	if ($("#hasMultipleResult").hasClass("hasMultipleResultAdj"))
		$("#hasMultipleResult").toggleClass("hasMultipleResultAdj");
		
	$("#hasMultipleResult").html("");
		
	if ($("#noResult").hasClass("noResultAdj"))
		$("#noResult").toggleClass("noResultAdj");
};

// Getting the background randomly.
var getRandomBg = function() {
	var imgCount = 4;
	var dir = "images/pics/";
	var randomCount = Math.round(Math.random() * (imgCount - 1)) + 1;
	var images = ["", "recife_001.jpg", "recife_002.jpg", "recife_003.jpg", "recife_004.jpg"];
	$(".backgroundImage").css("background-image", "url(" + dir + images[randomCount] + ")");
};

// Searching by the content.
var searchContent = function(content) {
	$.ajax({
		dataType: "json",
		url: "/search/" + content,
		success: function (data) { checkResultData(data); }
	});
};